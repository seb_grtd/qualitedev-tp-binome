import java.util.ArrayList;
class Thing{
    String name;
    int volume;
    public Thing(String name, int volume){
        this.name = name;
        this.volume = volume;
    }
    public String getName() {
        return name;
    }
    public int volume(){
        return this.volume;
    }
    @Override
    public boolean equals(Object o){
        if (o==this){return true;}
        if (o==null){return false;}
        if (o instanceof Box){
            Thing t= (Thing) o;
            return t.name.equals(this.name)&& t.volume() == this.volume();
        }
    return false;
    }
}

public class Box{
    public class ThingNotFound  extends Exception {}
    ArrayList<Thing> contents=new ArrayList<Thing>();
    
    private boolean open;
    private int capacite;
    public Box(){
        System.out.println("Box créée");
        this.open= true;
        this.capacite = -1;
    }
    public void add(Thing truc){
        this.contents.add(truc);
    }
    public boolean contient(Thing t){
        return this.contents.contains(t);
    }
    public Thing find(String nom)
        throws ThingNotFound
        {
            for (Thing t:contents){
                if (t.getName().equals(nom)){
                    return t;
                }
            }
        
        throw new ThingNotFound();
    
    }
    public void close(){
        this.open = false;
    }
    public void open(){
        this.open = true;
    }
    public boolean isOpen(){
        return this.open;
    }
    public String actionLook(){
        if (!this.isOpen())
            return "la boite est fermée";
        String res = "la boite contient: ";
        for (int i = 0; i < this.contents.size(); i++){
            res += this.contents.get(i).name;
            if (i != this.contents.size()-1)
                res += ", ";    
        }
        return res;
    }
    public void setCapacity(int NvCapacite){
        this.capacite = NvCapacite;
    }
    public int capacite(){
        return this.capacite;
    }
}
