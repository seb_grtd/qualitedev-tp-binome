
import org.junit.*;
import static org.junit.Assert.assertEquals;
public class TestsBoxes{
    @Test
    public void testBoxCreate() {
        Box b = new Box();
    }
    @Test
    public void testBoxAdd(){
        Box b = new Box();
        Thing telephone=new Thing("telephone", 4);
        Thing ordinateur=new Thing("ordinateur", 10);
        b.add(telephone);
        b.add(ordinateur);
    }
    @Test
    public void testBoxOpenClose(){
        Box box = new Box();
        box.add(new Thing("truc1", 5));
        box.add(new Thing("truc2", 6));
        box.close();
        assertEquals(box.actionLook(), "la boite est fermée");
        box.open();
        assertEquals(box.actionLook(), "la boite contient: truc1, truc2");
    }
    @Test
    public void testBoxVolume(){
        Box box = new Box();
        box.add(new Thing("truc1", 5));
        box.add(new Thing("truc2", 6));
        assert box.capacite() == 11;
    }
};

@Test
    public void testBoxFind()
        throws Box.ThingNotFound{
            Box b = new Box();
            Thing telephone=new Thing("telephone");
            Thing ordinateur=new Thing("ordinateur");
            b.add(telephone);
            b.add(ordinateur);
            assertThrows(Box.ThingNotFound.class,() -> b.find("maquette"));
            Thing myPhone=b.find("telephone");
            Thing maquette=b.find("maquette");
        }